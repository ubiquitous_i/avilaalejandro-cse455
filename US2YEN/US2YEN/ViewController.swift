//
//  ViewController.swift
//  US2YEN
//
//  Created by Alejandro Avila on 1/14/18.
//  Copyright © 2018 Alejandro Avila. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func Button1(_ sender: Any) {
        //Get the input from the Text Field
        let textFieldValue = Double(TextField1.text!)
        //if statement to make sure user cannot leave Text Field blank
        if textFieldValue != nil {
            let result = Double(textFieldValue! * 112.57)
            Label1.text = "$\(textFieldValue!) = ¥\(result)"
            //Clear Text Field after clicking the Button
            TextField1.text = ""
        } else {
            Label1.text = "This field cannot be blank!"
        }
    }
    @IBOutlet weak var Label1: UILabel!
    
    @IBOutlet weak var TextField1: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

